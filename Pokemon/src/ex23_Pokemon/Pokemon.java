package ex23_Pokemon;

public class Pokemon {
    int codigo;
    String nombre;
    String tipo1;
    String tipo2;
    int salud;
    int ataque;
    int defensa;
    int ataqueEspecial;
    int defensaEspecial;
    int agilidad;
    byte generacion;
    boolean legendario;



    public Pokemon(int codigo, String nombre, String tipo1, String tipo2, int salud, int ataque, int defensa, int ataqueEsp, int defensaEsp, int agilidad, Byte genByte, Boolean legendario) {
        setAgilidad(agilidad);
        setAtaque(ataque);
        setCodigo(codigo);
        setDefensa(defensa);
        setDefensaEspecial(defensaEspecial);
        setAtaqueEspecial(ataqueEspecial);
        setGeneracion(generacion);
        setLegendario(legendario);
        setTipo1(tipo1);
        setTipo2(tipo2);
        setNombre(nombre);
        setSalud(salud);

    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getAtaqueEspecial() {
        return ataqueEspecial;
    }

    public void setAtaqueEspecial(int ataqueEspecial) {
        this.ataqueEspecial = ataqueEspecial;
    }

    public int getDefensaEspecial() {
        return defensaEspecial;
    }

    public void setDefensaEspecial(int defensaEspecial) {
        this.defensaEspecial = defensaEspecial;
    }

    public int getAgilidad() {
        return agilidad;
    }

    public void setAgilidad(int agilidad) {
        this.agilidad = agilidad;
    }

    public byte getGeneracion() {
        return generacion;
    }

    public void setGeneracion(byte generacion) {
        this.generacion = generacion;
    }

    public boolean isLegendario() {
        return legendario;
    }

    public void setLegendario(boolean legendario) {
        this.legendario = legendario;
    }



}
