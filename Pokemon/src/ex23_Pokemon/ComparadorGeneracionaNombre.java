package ex23_Pokemon;

import java.util.Comparator;

public class ComparadorGeneracionaNombre implements Comparator<Pokemon> {

    @Override
    public int compare(Pokemon p1, Pokemon p2) {
        int comparadorGen = Byte.compare(p1.getGeneracion(), p2.getGeneracion());
        if (comparadorGen != 0) {
            return comparadorGen;
        }
        return p1.getNombre().compareTo(p2.getNombre());
    }
}
