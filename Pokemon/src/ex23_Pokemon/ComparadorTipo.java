package ex23_Pokemon;

import java.util.Comparator;

public class ComparadorTipo implements Comparator<Pokemon> {

    @Override
    public int compare(Pokemon p1, Pokemon p2) {
        int compararTipo1 = p1.getTipo1().compareTo(p2.getTipo1());
        if (compararTipo1 == 0) {
            return p1.getTipo2().compareTo(p2.getTipo2());
        }
        return compararTipo1;
    }
}
