package ex23_Pokemon;

import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class TestPokemon {
    private static List<Pokemon> pokemons;
    public static String PATH = "C:\\Users\\Fran Martinez\\IdeaProjects\\progamacion\\Pokemon\\src\\ex23_Pokemon\\";
    public static String FILE = PATH +"pokemons.csv";
    public static String FILE_TOTALS = PATH + "pokemonsTotals.csv";
    public static String FILE_SEARCH = PATH +"pokemonSearch.csv";
    public static String FILE_GENERATION = PATH +"pokemonGeneration";
    public static String FILE_LEGENDARY = PATH + "pokemonLegendary.csv";
    public static String FILE_GEN_NAME = PATH + "pokemonGenName.csv";
    public static String FILE_TYPE = PATH + "pokemonType.csv";
    public static String FILE_EXT = PATH +"pokemonExt.csv";


    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        new AdministradorArchivos();

        int opcion = menuPokemon();

        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    pokemonsTotales();
                    break;
                case 2:
                    buscarPokemon();
                    break;
                case 3:
                    extraerGeneracion();
                    break;
                case 4:
                    ordenLegendario();
                    break;
                case 5:
                    ordenTipos();
                    break;
                case 6:
                    ordenGeneracionNombre();
                    break;
                default:
                    System.out.println("Opcion no valida");
            }
            opcion = menuPokemon();
        }
        System.out.println("Conseguistes a todos!! Fin de Juego");
    }

    public static int menuPokemon() {
        Scanner leer = new Scanner(System.in);
        System.out.println("");
        System.out.println("--------|MENU|--------");
        System.out.println("1. TOTALS");
        System.out.println("2. BUSCAR POKEMON");
        System.out.println("3. EXTRAER GENERACION");
        System.out.println("4. ORDENAR LEGENDARIOS");
        System.out.println("5. ORDENAR POR TIPO1 Y TIPO2");
        System.out.println("6. ORDENAR POR GENERACION Y NOMBRE");
        System.out.println("0. SALIR");
        System.out.println("");
        System.out.println("   ELIGE UNA OPCION");
        System.out.println("----------------------");
        int opcion = leer.nextInt();
        return opcion;
    }

    public static Object pokemonsTotales() {
        Scanner leer = new Scanner(System.in);
        String tipo1, tipo2;
        ComparadorTipo comparadorTipo = new ComparadorTipo();
        AdministradorArchivos administradorArchivos = new AdministradorArchivos();
        //leer tipos
        int opcion;
        System.out.println("OPCION ELEGIDA: 1");
        System.out.println("TOTALS");
        System.out.println("ELIGE LA CATEGORIA DEL TIPO1: ");
        System.out.println("1.BUG  2.DARK  3.DRAGON  4.ELECTRIC  5.FAIRY");
        System.out.println("6.FIGHTING  7.FIRE  8.FLYING  9.GHOST  10.GRASS");
        System.out.println("11.GROUND  12.ICE  13.NORMAL  14.POISON  15.PSYCHIC");
        System.out.println("16.ROCK  17.STEEL  18.WATER");
        opcion = leer.nextInt();
        tipo1 = atributo(opcion);
        System.out.println("TOTALS");
        System.out.println("ELIGE LA CATEGORIA DEL TIPO1: ");
        System.out.println("1.BUG  2.DARK  3.DRAGON  4.ELECTRIC  5.FAIRY");
        System.out.println("6.FIGHTING  7.FIRE  8.FLYING  9.GHOST  10.GRASS");
        System.out.println("11.GROUND  12.ICE  13.NORMAL  14.POISON  15.PSYCHIC");
        System.out.println("16.ROCK  17.STEEL  18.WATER");
        opcion = leer.nextInt();
        tipo2 = atributo(opcion);
        String text = administradorArchivos.leerTotales( FILE , tipo1, tipo2);
        System.out.println(text.toUpperCase());
        administradorArchivos.escribir(text,  FILE_TOTALS);
        return administradorArchivos.toString();
    }

    public static String atributo(int opcion) {
        switch (opcion) {
            case 1: return "Bug";
            case 2: return "Dark";
            case 3: return "Dragon";
            case 4: return "Electric";
            case 5: return "Fairy";
            case 6: return "Fighting";
            case 7: return "Fire";
            case 8: return "Flying";
            case 9: return "Ghost";
            case 10: return "Grass";
            case 11: return "Ground";
            case 12: return "Ice";
            case 13: return "Normal";
            case 14: return "Poison";
            case 15: return "Psychic";
            case 16: return "Rock";
            case 17: return "Steel";
            case 18: return "Water";
            default: return null;
        }
    }

    public static String buscarPokemon(){
        Scanner leer = new Scanner(System.in);
        String nombre;
        AdministradorArchivos administradorArchivos = new AdministradorArchivos();
        System.out.println("OPCION ELEGIDA: 2");
        System.out.println("BUSQUEDA POKEMON");
        System.out.println("INGRESE EL NOMBRE DEL POKEMON: ");
        nombre = leer.nextLine();
        String text = administradorArchivos.buscarPokemon( FILE ,nombre);
        System.out.println(text.toUpperCase());
        administradorArchivos.escribir(text,  FILE_SEARCH);
        return administradorArchivos.toString();
    }

    public static String extraerGeneracion() {
        Scanner scanner = new Scanner(System.in);
        AdministradorArchivos administradorArchivos = new AdministradorArchivos();
        System.out.println("OPCIÓN ELEGIDA: 3");
        System.out.println("GENERACIÓN POKEMON");
        System.out.println("ELIGE UNA GENERACIÓN: ");
        byte generation = scanner.nextByte();
        pokemons = administradorArchivos.extraerGeneracion(FILE, generation);
        if (pokemons.isEmpty()) {
            System.out.println("El ARCHIVO pokemonGeneracion " + generation + ".csv no se ha creado.");
        } else {
            for (Pokemon pokemon : pokemons) {
                System.out.println(pokemon.getNombre().toUpperCase());
            }
            String archivoSalida = FILE_GENERATION + generation + ".csv";
            administradorArchivos.escribirGeneracion(pokemons, archivoSalida);
        }return administradorArchivos.toString();
    }

    public static void ordenGeneracionNombre(){
        AdministradorArchivos ordenarNombresGen = new AdministradorArchivos();
        System.out.println("OPCION ELEGIDA: 6");
        System.out.println("ORDENAR GENERACION Y NOMBRE");
        List<Pokemon> pokemons = ordenarNombresGen.ordenarTipo(FILE);
        String archivoSalida = FILE_GEN_NAME;
        ordenarNombresGen.escribirGeneracion(pokemons, archivoSalida);


    }

    public static void ordenTipos(){
        AdministradorArchivos ordenarTipos = new AdministradorArchivos();
        System.out.println("OPCION ELEGIDA: 5");
        System.out.println("ORDENAR TIPO1 Y TIPO2");
        List<Pokemon> pokemons = ordenarTipos.ordenarTipo(FILE);
        String archivoSalida = FILE_TYPE;
        ordenarTipos.escribirGeneracion(pokemons, archivoSalida);

    }

    public static void ordenLegendario(){
        Scanner leer = new Scanner(System.in);
        AdministradorArchivos ordenLegendario = new AdministradorArchivos();
        System.out.println("OPCION ELEGIDA: 4");
        System.out.println("ORDEN LEGENDARIOS:");
        System.out.println("ELEGIR 1.LEGENDARIOS 2.NO LEGENDARIOS: ");
        byte opcionLegendario = leer.nextByte();
        byte legendarioByte = (opcionLegendario == 1) ? (byte) 1 : (byte) 0;

        List<Pokemon> pokemons = ordenLegendario.ordenarLegendario(FILE, legendarioByte);

        String outputFile = FILE_LEGENDARY;
        ordenLegendario.escribirLegendario(pokemons, outputFile);

    }

}
