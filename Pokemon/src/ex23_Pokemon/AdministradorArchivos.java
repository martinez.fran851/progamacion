package ex23_Pokemon;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.nio.file.Files.delete;

public class AdministradorArchivos {

    //Metodo para leer los datos de un archivo y devolver un texto que contiene los pokemons y los tipos, con la suma de puntos.
    public String leerTotales(String nombreArchivo, String tipo1, String tipo2){
        StringBuilder totales = new StringBuilder();
        try(BufferedReader leer = new BufferedReader(new FileReader(nombreArchivo))) {
            String linea;
            while ((linea = leer.readLine()) != null){
                String[] datos = linea.split(";");
                if(datos.length >= 12){
                    String pokemontipo1 = datos[2];
                    String pokemontipo2 = datos[3];

                    if((tipo1.isEmpty() || pokemontipo1.equalsIgnoreCase(tipo1)) && (tipo2.isEmpty() || pokemontipo2.equalsIgnoreCase(tipo2))){
                       int salud = Integer.parseInt(datos[4]);
                       int ataque = Integer.parseInt(datos[5]);
                       int defensa = Integer.parseInt(datos[6]);
                       int ataqeEspecial = Integer.parseInt(datos[7]);
                       int defensaEspecial = Integer.parseInt(datos[8]);
                       int agilidad = Integer.parseInt(datos[9]);

                       int total = salud + ataque + defensa + ataqeEspecial + defensaEspecial + agilidad;

                       totales.append(datos[0]).append(";")
                               .append(datos[1]).append(";")
                               .append(pokemontipo1).append(";")
                               .append(pokemontipo2).append(";")
                               .append(total).append("\n");

                    }
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return totales.toString();

    }
    //Metodo para escribir los datos a un archivo de salida.
    public void escribir(String text, String archivonuevo) {
        try(BufferedWriter escribir = new BufferedWriter(new FileWriter(archivonuevo))) {
            escribir.write(text);
            System.out.println("FILE "+archivonuevo+ " CREATED.");
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    //Metodo para buscar pokemons
    public String buscarPokemon(String nombreArchivo,String nombre) {
        StringBuilder totales = new StringBuilder();
        try (BufferedReader leer = new BufferedReader(new FileReader(nombreArchivo))) {
            String linea;
            while ((linea = leer.readLine()) != null) {
                String[] datos = linea.split(";");
                if (datos.length >= 12) {
                    if(datos[1].toLowerCase().contains(nombre.toLowerCase())){
                        totales.append(datos[0]).append(";")
                                .append(datos[1]).append(";")
                                .append(datos[2]).append(";")
                                .append(datos[3]).append(";")
                                .append(datos[4]).append(";")
                                .append(datos[5]).append(";")
                                .append(datos[6]).append(";")
                                .append(datos[7]).append("\n");
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return totales.toString();

    }

    //Metodo para buscar la generacion
    public List<Pokemon> extraerGeneracion(String nombreArchivo, byte num_gen) {
        List<Pokemon> pokemons = new ArrayList<>();

        try (BufferedReader leer = new BufferedReader(new FileReader(nombreArchivo))) {

            String linea;
            leer.readLine();
            while ((linea = leer.readLine()) != null) {
                String[] datos = linea.split(";");
                byte pokeGeneracion = Byte.parseByte(datos[10]);
                if (pokeGeneracion == num_gen) {
                    int codigo = Integer.parseInt(datos[0]);
                    String nombre = datos[1];
                    String tipo1 = datos[2];
                    String tipo2 = datos[3];
                    int salud = Integer.parseInt(datos[4]);
                    int ataque = Integer.parseInt(datos[5]);
                    int defensa = Integer.parseInt(datos[6]);
                    int ataqueEsp = Integer.parseInt(datos[7]);
                    int defensaEsp = Integer.parseInt(datos[8]);
                    int agilidad = Integer.parseInt(datos[9]);
                    byte genByte = Byte.parseByte(datos[10]);
                    boolean legendario = Boolean.parseBoolean(datos[11]);

                    Pokemon pokemon = new Pokemon(codigo, nombre, tipo1, tipo2, salud, ataque, defensa, ataqueEsp, defensaEsp, agilidad, genByte, legendario);
                    pokemons.add(pokemon);
                }
            }
            System.out.println("Se han extraído los Pokémon de la generación " + num_gen);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pokemons;

    }
    public void escribirGeneracion(List<Pokemon> pokemons, String nombreArchivo) {
        if (pokemons.isEmpty()) {
            System.out.println("El ARCHIVO " + nombreArchivo + ".csv no se ha creado.");
        } else {
            try (BufferedWriter escribir = new BufferedWriter(new FileWriter(nombreArchivo))) {
                escribir.write("Code;Name;Type1;Type2;HealthPoints;Attack;Defense;SpecialAttack;SpecialDefense;Speed;Generation;Legendary");
                escribir.newLine();

                for (Pokemon pokemon : pokemons) {
                    escribir.write(pokemon.getCodigo() + ";" +
                            pokemon.getNombre() + ";" +
                            pokemon.getTipo1() + ";" +
                            pokemon.getTipo2() + ";" +
                            pokemon.getSalud() + ";" +
                            pokemon.getAtaque() + ";" +
                            pokemon.getDefensa() + ";" +
                            pokemon.getAtaqueEspecial() + ";" +
                            pokemon.getDefensaEspecial() + ";" +
                            pokemon.getAgilidad() + ";" +
                            pokemon.getGeneracion() + ";" +
                            pokemon.isLegendario());
                    escribir.newLine();
                }

            } catch (IOException e) {
                System.err.println("Error al escribir en el archivo: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public List<Pokemon> ordenarLegendario(String fileInput, byte legendary) {
        List<Pokemon> pokemons = new ArrayList<>();
        try (BufferedReader leer = new BufferedReader(new FileReader(fileInput))) {
            String linea;
            leer.readLine();
            while ((linea = leer.readLine()) != null) {
                String[] datos = linea.split(";");
                boolean isLegendary = Boolean.parseBoolean(datos[11]);
                if (isLegendary == (legendary == 1)) {
                    int codigo = Integer.parseInt(datos[0]);
                    String nombre = datos[1];
                    String tipo1 = datos[2];
                    String tipo2 = datos[3];
                    int salud = Integer.parseInt(datos[4]);
                    int ataque = Integer.parseInt(datos[5]);
                    int defensa = Integer.parseInt(datos[6]);
                    int ataqueEsp = Integer.parseInt(datos[7]);
                    int defensaEsp = Integer.parseInt(datos[8]);
                    int agilidad = Integer.parseInt(datos[9]);
                    byte generacion = Byte.parseByte(datos[10]);
                    boolean legendario = Boolean.parseBoolean(datos[11]);

                    Pokemon pokemon = new Pokemon(codigo, nombre, tipo1, tipo2, salud, ataque, defensa, ataqueEsp, defensaEsp, agilidad, generacion, legendario);
                    pokemons.add(pokemon);
                }
            }
            System.out.println("Se han extraído los Pokémon " + (legendary == 1 ? "legendarios" : "no legendarios") );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pokemons;
    }

    public void escribirLegendario(List<Pokemon> pokemons, String nombreArchivo) {
        if (pokemons.isEmpty()) {
            System.out.println("El archivo " + nombreArchivo + " no se ha creado debido a que la lista de Pokémon está vacía.");
        } else {
            try (BufferedWriter escribir = new BufferedWriter(new FileWriter(nombreArchivo))) {
                escribir.write("Code;Name;Type1;Type2;HealthPoints;Attack;Defense;SpecialAttack;SpecialDefense;Speed;Generation;Legendary");
                escribir.newLine();

                for (Pokemon pokemon : pokemons) {
                    escribir.write(pokemon.getCodigo() + ";" +
                            pokemon.getNombre() + ";" +
                            pokemon.getTipo1() + ";" +
                            pokemon.getTipo2() + ";" +
                            pokemon.getSalud() + ";" +
                            pokemon.getAtaque() + ";" +
                            pokemon.getDefensa() + ";" +
                            pokemon.getAtaqueEspecial() + ";" +
                            pokemon.getDefensaEspecial() + ";" +
                            pokemon.getAgilidad() + ";" +
                            pokemon.getGeneracion() + ";" +
                            pokemon.isLegendario());
                    escribir.newLine();
                }

                System.out.println("Archivo " + nombreArchivo + " se creo.");
            } catch (IOException e) {
                System.err.println("Error al escribir en el archivo: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    public List<Pokemon> ordenarTipo(String nombreArchivo) {
        List<Pokemon> pokemons = new ArrayList<>();
        try (BufferedReader leer = new BufferedReader(new FileReader(nombreArchivo))) {
            String linea;
            leer.readLine();
            while ((linea = leer.readLine()) != null) {
                String[] datos = linea.split(";");
                int codigo = Integer.parseInt(datos[0]);
                String nombre = datos[1];
                String tipo1 = datos[2];
                String tipo2 = datos[3];
                int salud = Integer.parseInt(datos[4]);
                int ataque = Integer.parseInt(datos[5]);
                int defensa = Integer.parseInt(datos[6]);
                int ataqueEsp = Integer.parseInt(datos[7]);
                int defensaEsp = Integer.parseInt(datos[8]);
                int agilidad = Integer.parseInt(datos[9]);
                byte generacion = Byte.parseByte(datos[10]);
                boolean legendario = Boolean.parseBoolean(datos[11]);

                Pokemon pokemon = new Pokemon(codigo, nombre, tipo1, tipo2, salud, ataque, defensa, ataqueEsp, defensaEsp, agilidad, generacion, legendario);
                pokemons.add(pokemon);
            }
            System.out.println("Se han extraído los Pokémon del archivo.");
        } catch (IOException e) {
            e.printStackTrace();
        }


        ComparadorTipo comparadorTipo = new ComparadorTipo();
        pokemons.sort(comparadorTipo);
        return pokemons;
    }

    public void escribir(List<Pokemon> pokemons, String archivoNombre) {
        if (pokemons.isEmpty()) {
            System.out.println("El archivo " + archivoNombre + " no se ha creado debido a que la lista de Pokémon está vacía.");
        } else {
            try (BufferedWriter escribir = new BufferedWriter(new FileWriter(archivoNombre))) {
                escribir.write("Code;Name;Type1;Type2;HealthPoints;Attack;Defense;SpecialAttack;SpecialDefense;Speed;Generation;Legendary");
                escribir.newLine();

                for (Pokemon pokemon : pokemons) {
                    escribir.write(pokemon.getCodigo() + ";" +
                            pokemon.getNombre() + ";" +
                            pokemon.getTipo1() + ";" +
                            pokemon.getTipo2() + ";" +
                            pokemon.getSalud() + ";" +
                            pokemon.getAtaque() + ";" +
                            pokemon.getDefensa() + ";" +
                            pokemon.getAtaqueEspecial() + ";" +
                            pokemon.getDefensaEspecial() + ";" +
                            pokemon.getAgilidad() + ";" +
                            pokemon.getGeneracion() + ";" +
                            pokemon.isLegendario());
                    escribir.newLine();
                }

                System.out.println("Archivo " + archivoNombre + " creado correctamente.");
            } catch (IOException e) {
                System.err.println("Error al escribir en el archivo: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    public List<Pokemon> ordenarGenNombre(String archivoNombre) {
        List<Pokemon> pokemons = new ArrayList<>();
        try (BufferedReader leer = new BufferedReader(new FileReader(archivoNombre))) {
            String linea;
            leer.readLine(); 
            while ((linea = leer.readLine()) != null) {
                String[] datos = linea.split(";");
                int codigo = Integer.parseInt(datos[0]);
                String nombre = datos[1];
                String tipo1 = datos[2];
                String tipo2 = datos[3];
                int salud = Integer.parseInt(datos[4]);
                int ataque = Integer.parseInt(datos[5]);
                int defensa = Integer.parseInt(datos[6]);
                int ataqueEsp = Integer.parseInt(datos[7]);
                int defensaEsp = Integer.parseInt(datos[8]);
                int agilidad = Integer.parseInt(datos[9]);
                byte generacion = Byte.parseByte(datos[10]);
                boolean legendario = Boolean.parseBoolean(datos[11]);

                Pokemon pokemon = new Pokemon(codigo, nombre, tipo1, tipo2, salud, ataque, defensa, ataqueEsp, defensaEsp, agilidad, generacion, legendario);
                pokemons.add(pokemon);
            }
            System.out.println("Se han extraído los Pokémon del archivo.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        ComparadorGeneracionaNombre comparadorGeneracionaNombre = new ComparadorGeneracionaNombre();
        pokemons.sort(comparadorGeneracionaNombre);
        

        return pokemons;
    }

    public void write(List<Pokemon> pokemons, String nombreArchivo) {
        if (pokemons.isEmpty()) {
            System.out.println("El archivo " + nombreArchivo + " no se ha creado debido a que la lista de Pokémon está vacía.");
        } else {
            try (BufferedWriter escribir = new BufferedWriter(new FileWriter(nombreArchivo))) {
                escribir.write("Code;Name;Type1;Type2;HealthPoints;Attack;Defense;SpecialAttack;SpecialDefense;Speed;Generation;Legendary");
                escribir.newLine();

                for (Pokemon pokemon : pokemons) {
                    escribir.write(pokemon.getCodigo() + ";" +
                            pokemon.getNombre() + ";" +
                            pokemon.getTipo1() + ";" +
                            pokemon.getTipo2() + ";" +
                            pokemon.getSalud() + ";" +
                            pokemon.getAtaque() + ";" +
                            pokemon.getDefensa() + ";" +
                            pokemon.getAtaqueEspecial() + ";" +
                            pokemon.getDefensaEspecial() + ";" +
                            pokemon.getAgilidad() + ";" +
                            pokemon.getGeneracion() + ";" +
                            pokemon.isLegendario());
                    escribir.newLine();
                }

                System.out.println("Archivo " + nombreArchivo + " creado correctamente.");
            } catch (IOException e) {
                System.err.println("Error al escribir en el archivo: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    



}
